/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity, Vibration, Alert} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const candidatos = [
  {
    nome: 'Bolsonaro',
    numero: {
      dezena: 1,
      unidade: 7
    }
  },
  {
    nome: 'Lula',
    numero: {
      dezena: 1,
      unidade: 3
    }
  },
  {
    nome: 'Voto em branco',
    numero: {
      dezena: 0,
      unidade: 0
    }
  }
];

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dezena: null, 
      unidade: null,
      candidato: null
    }
  }

  _findCandidato(dezena, unidade){
    const candidato = candidatos.find((value, index) => {
      return value.numero.dezena === dezena && value.numero.unidade === unidade;
    });

    if(candidato){
      return candidato;
    }else{
      Alert.alert(
        "Candidato não encontrado!",
        null, 
        [
          {
            text: 'OK',
            onPress: () => {
              this.setState({dezena: null, unidade: null, candidato: null});
            }
          }
        ],
        {
          cancelable: false
        }
      );
      return null;
    }
  }

  _clickKeyboard(value){
    console.log(value);
    console.log(this.state);
    Vibration.vibrate(100);
    if(value !== "branco" && value !== "corrige" && value !== "confirma"){
      if(this.state.dezena === null && this.state.unidade === null){
        this.setState({
          dezena: value
        });
      }else if(this.state.dezena !== null && this.state.unidade === null){
        this.setState({
          unidade: value
        });
        setTimeout(() => {
          this.setState({candidato: this._findCandidato(this.state.dezena, this.state.unidade)});
          clearTimeout();
          console.log(this.state.candidato);
        }, 100);
      }
    }else if(value === "corrige"){
      this.setState({
        dezena: null, 
        unidade: null,
        candidato: null
      });
    }else if(value == "branco"){
      this.setState({
        dezena: 0,
        unidade: 0
      });
    }else if(value === "confirma"){
      Alert.alert("Voto confirmado!");
      this.setState({
        dezena: null, 
        unidade: null,
        candidato: null
      });
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerNumbers}>
            <View style={styles.headerNumbersNumber}>
              <Text style={styles.headerNumbersNumberText}>
                {this.state.dezena}
              </Text>
            </View>
            <View style={styles.headerNumbersNumber}>
              <Text style={styles.headerNumbersNumberText}>
                {this.state.unidade}
              </Text>
            </View>
          </View>
          {this.state.candidato != null ? 
          <View style={styles.candidato}>
            <Text style={styles.candidatoNome}>
              {this.state.candidato.nome}
            </Text>
          </View>
          : null }
        </View>
        <View style={styles.keyboard}>
          <View style={styles.keyboardRow}>
            <TouchableOpacity onPress={() => this._clickKeyboard(1)} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonFirstRow]} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                1
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard(2)} activeOpacity={0.2} style={styles.keyboardButton} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                2
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard(3)} activeOpacity={0.2} style={styles.keyboardButton} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                3
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.keyboardRow}>
            <TouchableOpacity onPress={() => this._clickKeyboard(4)} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonFirstRow]} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                4
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard(5)} activeOpacity={0.2} style={styles.keyboardButton} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                5
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard(6)} activeOpacity={0.2} style={styles.keyboardButton} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                6
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.keyboardRow}>
            <TouchableOpacity onPress={() => this._clickKeyboard(7)} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonFirstRow]} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                7
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard(8)} activeOpacity={0.2} style={styles.keyboardButton} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                8
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard(9)} activeOpacity={0.2} style={styles.keyboardButton} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                9
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.keyboardRow}>
            <TouchableOpacity onPress={() => this._clickKeyboard(0)} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonFirstRow]} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={styles.keyboardButtonText}>
                0
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.keyboardRow}>
            <TouchableOpacity onPress={() => this._clickKeyboard("branco")} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonFirstRow, styles.keyboardButtonBranco]} disabled={this.state.dezena !== null && this.state.unidade !== null}>
              <Text style={[styles.keyboardButtonText, styles.keyboardButtonBrancoText]}>
                Branco
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard("corrige")} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonCorrige]} disabled={!(this.state.dezena !== null || this.state.unidade !== null)}>
              <Text style={[styles.keyboardButtonText, styles.keyboardButtonCorrigeText]}>
                Corrige
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._clickKeyboard("confirma")} activeOpacity={0.2} style={[styles.keyboardButton, styles.keyboardButtonConfirma]} disabled={(this.state.dezena === null || this.state.unidade === null)}>
              <Text style={[styles.keyboardButtonText, styles.keyboardButtonConfirmaText]}>
                Confirma
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  header: {
    flex: 1,
    padding: 16
  },
  keyboard: {
    flex: 4,
    backgroundColor: '#000000'
  },
  keyboardRow: {
    flexDirection: 'row',
    padding: 8,
    flex: 1
  },
  keyboardButton: {
    elevation: 3,
    flex: 1,
    padding: 16,
    shadowColor: '#FFFFFF',
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 8
  },
  keyboardButtonFirstRow: {
    marginLeft: 0
  },
  keyboardButtonText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FFF'
  },
  keyboardButtonBranco: {
    backgroundColor: '#FFF',
  },
  keyboardButtonBrancoText: {
    color: '#000',
    fontSize: 15,
    textTransform: 'uppercase'
  },
  keyboardButtonCorrige: {
    backgroundColor: 'orange',
  },
  keyboardButtonCorrigeText: {
    color: '#000',
    fontSize: 15,
    textTransform: 'uppercase'
  },
  keyboardButtonConfirma: {
    backgroundColor: 'green'
  },
  keyboardButtonConfirmaText: {
    color: '#000',
    fontSize: 15,
    textTransform: 'uppercase'
  },
  headerNumbers: {
    flexDirection: 'row',
    flex: 2
  },
  headerNumbersNumber: {
    padding: 16,
    backgroundColor: '#D0D3D4',
    borderColor: '#B3B6B7',
    borderWidth: 1,
    margin: 8,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  headerNumbersNumberText: {
    color: '#4D5656',
    fontSize: 30,
    fontWeight: 'bold'
  },
  candidato: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  candidatoNome: {
    color: '#4D5656',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase'
  }
});
